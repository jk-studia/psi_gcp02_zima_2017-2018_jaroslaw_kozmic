import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.BackPropagation;
import org.neuroph.util.TransferFunctionType;

public class NueralNetwork {

    public static void main(String[] args) {
        /*
        * Sieć neuronowa wykorzystująca algorytm wstecznej propagacji
        * Wykorzystano biblioteke neuroph wersja: 2.92
        *
        * */

        // Utworzenie danych uczących
        DataSet trainingData = new DataSet(35, 20);
        trainingData.setLabel("TrainingData");
        trainingData.addRow(new DataSetRow(new double[]{0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1}, new double[]{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));
        trainingData.addRow(new DataSetRow(new double[]{1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0}, new double[]{0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));
        trainingData.addRow(new DataSetRow(new double[]{0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0}, new double[]{0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));
        trainingData.addRow(new DataSetRow(new double[]{1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0}, new double[]{0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));
        trainingData.addRow(new DataSetRow(new double[]{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1}, new double[]{0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));
        trainingData.addRow(new DataSetRow(new double[]{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0}, new double[]{0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));
        trainingData.addRow(new DataSetRow(new double[]{0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0}, new double[]{0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));
        trainingData.addRow(new DataSetRow(new double[]{1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1}, new double[]{0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));
        trainingData.addRow(new DataSetRow(new double[]{0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0}, new double[]{0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));
        trainingData.addRow(new DataSetRow(new double[]{1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0}, new double[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));
        trainingData.addRow(new DataSetRow(new double[]{1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1}, new double[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0}));
        trainingData.addRow(new DataSetRow(new double[]{1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1}, new double[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0}));
        trainingData.addRow(new DataSetRow(new double[]{1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1}, new double[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0}));
        trainingData.addRow(new DataSetRow(new double[]{1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1}, new double[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0}));
        trainingData.addRow(new DataSetRow(new double[]{0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0}, new double[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0}));
        trainingData.addRow(new DataSetRow(new double[]{1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0}, new double[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0}));
        trainingData.addRow(new DataSetRow(new double[]{0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1}, new double[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0}));
        trainingData.addRow(new DataSetRow(new double[]{1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1}, new double[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0}));
        trainingData.addRow(new DataSetRow(new double[]{0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0}, new double[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0}));
        trainingData.addRow(new DataSetRow(new double[]{1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0}, new double[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}));

        // Utworzenie klasy zawierającej algorytm wstecznej propagacji oraz ustawienie maksymalnego błędu i stałej uczenia
        BackPropagation backPropagation = new BackPropagation();
        backPropagation.setMaxError(0.01);
        backPropagation.setLearningRate(0.1);

        // Utworzenie sieci neuronowej skłądającej się z: 35 neuronów wejściowych, 10 neuronów ukrytych, 20 nueronów zwracających wynik
        // Neurony mają funkcję aktywacji sigmoidalną oprócz neuronów wejściowych, które mają liniową (przekazują jedynie dane wejściowe bez wag do warstwy ukrytej)
        MultiLayerPerceptron multiLayerPerceptron = new MultiLayerPerceptron(TransferFunctionType.SIGMOID, 35,10,20);
        multiLayerPerceptron.setLabel("MyNetwork");

        // Ustawienie algorytmu wstecznej propagacji jako algorytmu rozwiązywania sieci neuronowej
        multiLayerPerceptron.setLearningRule(backPropagation);

        // Uczenie sieci oraz zapisane do pliku "my.nnet"
        System.out.println("Teaching: "+multiLayerPerceptron.getLabel()+", with data set: "+trainingData.getLabel());
        System.out.println("Using algorithm backPropagation with configuration: \n Max Error: "+backPropagation.getMaxError()+"\n Learning Rate: "+backPropagation.getLearningRate());
        multiLayerPerceptron.learn(trainingData);
        System.out.println("Teaching is finished:");
        // Wyswietlenie liczby iteracji oraz całkowitego błędu uczenia w sieci neuronowej
        System.out.println(" Number of iterations: "+ backPropagation.getCurrentIteration());
        System.out.println(" Total error: " + backPropagation.getErrorFunction().getTotalError());

        multiLayerPerceptron.save("my.nnet");
        // Utworzenie danych testujących
     /*   DataSet testingData = new DataSet(35, 20);
        testingData.setLabel("TestingData");
        testingData.addRow(trainingData.getRowAt(1));
        testingData.addRow(trainingData.getRowAt(10));
        testingData.addRow(trainingData.getRowAt(19));
        testingData.addRow(trainingData.getRowAt(15));
        testingData.addRow(trainingData.getRowAt(5));
        opcjonalnie
        */


        // wczytanie utworzonej sieci
        NeuralNetwork neuralNetwork = NeuralNetwork.createFromFile("my.nnet");

        // przeprowadzenie testu sieci i porównanie żądanego outputu z otrzymanym
        int letter = 1;
        for(DataSetRow dataSetRow: trainingData.getRows()){
            System.out.println();
            System.out.println();
            System.out.println("Letter: " +letter);
            double[] desiredOutput = dataSetRow.getDesiredOutput();

            neuralNetwork.setInput(dataSetRow.getInput());
            neuralNetwork.calculate();
            double[] output = neuralNetwork.getOutput();

            System.out.println("Desired output:");
            for(int i = 0;  i < 20; i++){
                System.out.print(desiredOutput[i]+ "  ");
            }
            System.out.println();
            System.out.println("Desired Letter: "+ checkLetter(desiredOutput));

            System.out.println("Output:");
            for(int i = 0;  i < 20; i++){
                System.out.print(output[i]+ "  ");
            }
            System.out.println();
            System.out.println("Your Letter: "+ checkLetter(output));

            letter++;
        }
    }

    public static char checkLetter(double[] letter){
        char yourLetter = 'X';
        double point = letter[0];
        int index = 0;

        // ustalenie litery 
        for(int i = 1; i < 20; i++){
            if(point < letter[i]){
                point = letter[i];
                index = i;
            }
        }
        switch(index){
            case 0: yourLetter = 'A'; break;
            case 1: yourLetter = 'B'; break;
            case 2: yourLetter = 'C'; break;
            case 3: yourLetter = 'D'; break;
            case 4: yourLetter = 'E'; break;
            case 5: yourLetter = 'F'; break;
            case 6: yourLetter = 'G'; break;
            case 7: yourLetter = 'H'; break;
            case 8: yourLetter = 'I'; break;
            case 9: yourLetter = 'J'; break;
            case 10: yourLetter = 'K'; break;
            case 11: yourLetter = 'L'; break;
            case 12: yourLetter = 'M'; break;
            case 13: yourLetter = 'N'; break;
            case 14: yourLetter = 'O'; break;
            case 15: yourLetter = 'P'; break;
            case 16: yourLetter = 'Q'; break;
            case 17: yourLetter = 'R'; break;
            case 18: yourLetter = 'S'; break;
            case 19: yourLetter = 'T'; break;
        }
        return yourLetter;
    }
}
