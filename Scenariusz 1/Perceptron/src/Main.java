import java.util.Scanner;

public class Main {

    public static void main(String[] args){
        System.out.println("Sign checking neuron");
        System.out.println("=========================\n");
        Scanner scan = new Scanner(System.in);
        Perceptron perceptron = new Perceptron();
        Neuron neuron = new Neuron();

        perceptron.setNeuron(neuron);
        perceptron.teach();

        Test test = new Test(neuron, scan);
        test.generate();
        test.beginTest();
        int n;
        do {
            System.out.println("Do you want to make test again?\n1.Yes\n2.No");
            n = scan.nextInt();
            if (n==1){
                test.generate();
                test.beginTest();
            }
            else {
                System.out.println("Thank you");
                scan.close();
            }
        } while(n==1);
    }
}
