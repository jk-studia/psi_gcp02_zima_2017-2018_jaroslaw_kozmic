import java.util.*;

public class Test {

    private int numberOfInputs;
    private Neuron neuron;
    private Random generator = new Random();
    private double inputs[][];
    private Scanner scan;

    public Test(Neuron neuron, Scanner scan){
        this.neuron = neuron;
        this.scan = scan;
    }

    public void generate(){
        System.out.println("Put number of testing inputs: ");
        numberOfInputs = scan.nextInt();
        inputs = new double[numberOfInputs][2];
        for(int i = 0; i < numberOfInputs; i++){
            for(int j = 0; j < 2;j++)
                inputs[i][j] = generator.nextDouble()*20-15;
        }
        System.out.println("Testing inputs:");
        for(int i = 0; i < numberOfInputs; i++) {
            System.out.println("Pair number: "+(i+1));
            for (int j = 0; j < 2; j++)
                System.out.println("    "+inputs[i][j]+ " ");
        }
    }

    public void beginTest(){
        double sign = 0.0;

        for(int i = 0; i < numberOfInputs; i++){
            neuron.setDendrites(inputs[i]);
            sign = neuron.axon();
            System.out.println("Pair number: "+(i+1));
        //    System.out.println(+neuron.getDendrites()[0]+"\n"+neuron.getDendrites()[1]);
            System.out.println("Sign: "+sign+"\n");
        }
    }
}
