import java.util.ArrayList;
import java.util.List;

public class Neuron {
    /* Legenda:
        dentrites = dentryty = wejście neuronu
        sinapses = synapsy = wagi odpowiednich wejść
        core = jądro = blok sumujący
        axonHillock = wzgórek aksonu = blok aktywacji
        axon = akson = wyjście neuronu

        Neuron McCullocha-Pittsa. Jest on podstawową częścią perceptronu, w tym programie rozdzielony od części uczącej.
    */
    private double[] dendrites;
    private double[] sinapses;

    public Neuron(){
        dendrites = new double[2];
        sinapses = new double[2];
    }

    public double core(){
        double sum = 0;
        for(int i = 0; i < 2; i++){
            sum += dendrites[i]*sinapses[i];
        }
        return sum;
    }

    public void setDendrites(double[] dendrites) {
        this.dendrites = dendrites;
    }

    public void setSinapses(double[] sinapses) {
        this.sinapses = sinapses;
    }

    public double[] getDendrites() {
        return dendrites;
    }

    public double[] getSinapses() {
        return sinapses;
    }

    public double axonHillock(double coreResult){
        if(coreResult >= 0) return 1;
        else return -1;
    }

    public double axon(){
        return axonHillock(core());
    }
}
