import java.util.*;

public class Perceptron {

    /*Legenda:
        C = stała uczenia
        generator = funkcja generująca losowe wagi
        weights = wagi neuronu
        inputs          }
                        } = dane służące do uczenia neuronu
        correctResults  }

        neuron = używany neuron

        W podanym programie klasa perceptron służy jako nauczyciel dla osobnego neuronu McCullocha-Pittsa.
    * */

    private final double C=0.1;

    private int numberOfInputs = 4;

    private double [][]inputs = {{-1,3},{-0.8,1},{1,0.2},{0.5,-0.75},{-2,4},{5,3},{-4,-4},{1,0}};
    private double[] weights;
    private double []correctResults = {1,-1,1,-1,1,1,-1,1};
    private Random generator = new Random();
    private Neuron neuron;

    public Perceptron(){
        weights = new double[2];
        generate();
    }

    public void generate(){
        for(int i = 0; i < 2; i++)
            weights[i]=generator.nextDouble()*2-1;
        System.out.println("Random Weights:");
        for(double x: weights)
            System.out.println(x);
    }
    public void setNeuron(Neuron neuron){
        this.neuron = neuron;
        neuron.setSinapses(weights);
    }

    public void teach(){
        System.out.println("\nTeaching neuron...\n");

        double result = 0;
        boolean check = false;
        int j = 0;
        do{
            j++;
            check = false;
            for(int i = 0; i < numberOfInputs; i++){
                neuron.setDendrites(inputs[i]);
                result = neuron.axon();
                if(correctResults[i]!=result){
                    neuron.getSinapses()[0] += C*(correctResults[i]-result)*neuron.getDendrites()[0];
                    neuron.getSinapses()[1] += C*(correctResults[i]-result)*neuron.getDendrites()[1];
                    check = true;
                }
            }
        }while(check == true);
        System.out.println("Correct Weights:");
        for(double x: neuron.getSinapses())
            System.out.println(x);
        System.out.println("Your neuron is ready, number of iterations: "+ j);
    }
}
