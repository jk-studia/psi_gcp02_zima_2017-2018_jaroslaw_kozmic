import java.util.*;

public class Test {

    private int numberOfInputs = 10;
    private Neuron neuron;
    private Random generator = new Random();
    private double inputs[][];
    private double []outputs;
    public Test(Neuron neuron){
        this.neuron = neuron;
    }

    public void generate(){
        TestingData data = new TestingData();
        data.readData();
        inputs = data.letters;
        outputs = data.outputs;
    }

    public void beginTest(){
        double sign;
        for(int i = 0; i < numberOfInputs; i++){
            neuron.setDendrites(inputs[i]);
            sign = neuron.axon();
            System.out.println("Letter: "+(i+1));
        //    System.out.println(+neuron.getDendrites()[0]+"\n"+neuron.getDendrites()[1]);
            System.out.println("Sign: "+sign+"\n");
        }
    }
}
