import java.io.IOException;
import java.util.*;

public class Perceptron {

    /*Legenda:
        C = stała uczenia
        generator = funkcja generująca losowe wagi
        weights = wagi neuronu
        inputs          }
                        } = dane służące do uczenia neuronu
        correctResults  }

        neuron = używany neuron

        W podanym programie klasa perceptron służy jako nauczyciel dla osobnego neuronu McCullocha-Pittsa.
    * */

    private final double C=0.01;

    private int numberOfInputs = 20;
    private double acceptableError = 0.1;
    private double [][]inputs = new double[20][20];
    private double[] weights;
    private double []correctResults = new double[20];
    private Random generator = new Random();
    private Neuron neuron;
    private double bias;

    public Perceptron(){
        weights = new double[20];
        generate();
        bias = Math.random();
    }

    public void generate(){
        for(int i = 0; i < weights.length; i++)
            weights[i]=generator.nextDouble()*2-1;
    }
    public void setNeuron(Neuron neuron){
        this.neuron = neuron;
        neuron.setSinapses(weights);
    }

    public void getData(){
        LearningData data = new LearningData();
        data.readData();
        inputs = data.letters;
        correctResults = data.outputs;
    }
    public void teach(){
        System.out.println("\nTeaching neuron...\n");


      double result;
      int amountOfIterartions = 0;
      boolean check;
      do{
          check = false;
          for(int i = 0; i < numberOfInputs; i++){
              neuron.setDendrites(inputs[i]);
              result = neuron.axon();
              double delta = correctResults[i]-result;
              if(delta>acceptableError){
                  for(int k = 0; k < 20; k++) {
                      neuron.getSinapses()[k] += C * (correctResults[i] - result) * neuron.getDendrites()[k];
                  }

                  check = true;
              }
          }
        amountOfIterartions++;
      }while(check==true);

      System.out.println("Your neuron is ready, number of iterations: "+amountOfIterartions);
    }
}
