import java.util.Scanner;

public class Main {

    public static void main(String[] args){
        Perceptron perceptron = new Perceptron();
        Neuron neuron = new Neuron();
        perceptron.setNeuron(neuron);
        perceptron.getData();
        perceptron.teach();

        Test test = new Test(neuron);
        test.generate();
        test.beginTest();
    }
}
