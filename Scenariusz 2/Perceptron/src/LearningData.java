import java.io.*;

public class LearningData {

    private File file = new File("learningData.txt");
    private BufferedReader bufferedReader;
    public double [][]letters = new double[20][20];
    public double []outputs = new double[20];
    private String line = null;
    private String []a = new String[20];

    public LearningData(){
        try{
            bufferedReader = new BufferedReader(new FileReader(file));
        }catch(FileNotFoundException e){
            System.out.println("Error: data not found");
        }
    }

    public void readData(){
        try {
            int number = 0;
            int i = 0;
            while((line = bufferedReader.readLine())!= null) {
                a = line.split(";");
                for (int j = 0; j < a.length; j++) {
                    number = Integer.valueOf(a[j]);
                    if (j != a.length - 1) letters[i][j] = number;
                    else outputs[i] = number;
                }
                i++;
            }


        }catch(IOException e){
            System.out.println("Error: reading line");
        }
    }

    void showLetters(){
        for(int o = 0; o < letters.length; o++){
            int l = 1;
            for(int k = 0; k < letters[o].length; k++){
                System.out.print(letters[o][k]);
                l++;
                if(l==5) {System.out.println(); l=1;}
            }
            System.out.println();
        }
    }
    void showOutputs(){
        for (int l = 0 ; l < outputs.length; l++){
            System.out.println(outputs[l]);
        }
    }

}
