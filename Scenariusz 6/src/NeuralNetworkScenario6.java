import org.neuroph.core.data.BufferedDataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.nnet.Kohonen;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class NeuralNetworkScenario6 {
    public static void main(String[] args){
        File file = new File("data.txt");
        BufferedDataSet bufferedDataSet;
        try {
            bufferedDataSet = new BufferedDataSet(file, 35, 35, " ");
            Kohonen kohonen = new Kohonen(35, 35);
            WTMLearningRule wtmLearningRule = new WTMLearningRule();
            kohonen.setLearningRule(wtmLearningRule);
            kohonen.learn(bufferedDataSet);

            List<Double> winners = new ArrayList<>();

            for(DataSetRow r: bufferedDataSet.getRows()){
                kohonen.setInput(r.getInput());
                kohonen.calculate();
                double[] output = kohonen.getOutput();
                double tmp = 0.0;
                double winner = 0.0;
                for(int i = 0; i < output.length; i++){
                    if(output[i] > tmp){
                        tmp = output[i];
                        winner = i;
                    }
                }
                winners.add(winner);
            }

            System.out.println("Results:");
            for(int i = 0; i < 20; i++){
                if(i == 0) System.out.print("A: "+winners.get(i)+"\n");
                if(i == 1) System.out.print("B: "+winners.get(i)+"\n");
                if(i == 2) System.out.print("C: "+winners.get(i)+"\n");
                if(i == 3) System.out.print("D: "+winners.get(i)+"\n");
                if(i == 4) System.out.print("E: "+winners.get(i)+"\n");
                if(i == 5) System.out.print("F: "+winners.get(i)+"\n");
                if(i == 6) System.out.print("G: "+winners.get(i)+"\n");
                if(i == 7) System.out.print("H: "+winners.get(i)+"\n");
                if(i == 8) System.out.print("I: "+winners.get(i)+"\n");
                if(i == 9) System.out.print("J: "+winners.get(i)+"\n");
                if(i == 10) System.out.print("K: "+winners.get(i)+"\n");
                if(i == 11) System.out.print("L: "+winners.get(i)+"\n");
                if(i == 12) System.out.print("M: "+winners.get(i)+"\n");
                if(i == 13) System.out.print("N: "+winners.get(i)+"\n");
                if(i == 14) System.out.print("O: "+winners.get(i)+"\n");
                if(i == 15) System.out.print("P: "+winners.get(i)+"\n");
                if(i == 16) System.out.print("Q: "+winners.get(i)+"\n");
                if(i == 17) System.out.print("R: "+winners.get(i)+"\n");
                if(i == 18) System.out.print("S: "+winners.get(i)+"\n");
                if(i == 19) System.out.print("T: "+winners.get(i)+"\n");
            }

            int numberOfGroups = 1;
            Collections.sort(winners);
            for(int i = 0; i < 19; i++){
                if(winners.get(i) < winners.get(i+1)) numberOfGroups++;
            }

            System.out.println("Number of Groups: "+numberOfGroups);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        }
}
