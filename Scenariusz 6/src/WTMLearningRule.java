import org.neuroph.core.Connection;
import org.neuroph.core.Layer;
import org.neuroph.core.Neuron;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.core.learning.LearningRule;

import java.util.Iterator;

public class WTMLearningRule extends LearningRule{

    private static final long serialVersionUID = 1L;

    private double learningRate = 0.1;
    private int iteration = 1000;
    private double neighborhoodRadius = 34;
    private int count = 0;

    public WTMLearningRule(){
        super();
    }

    @Override
    public void learn(DataSet trainingSet) {
        System.out.println("Teaching in progress...");
        System.out.println("Number of iterations: "+iteration);
        System.out.println("Starting neighborhood radius: "+neighborhoodRadius);
        System.out.println("Starting learning rate: "+learningRate);
        for(int i = 0; i < iteration; i++) {
            Iterator<DataSetRow> iterator = trainingSet.iterator();
            while (iterator.hasNext() && !isStopped()) {
                DataSetRow dataSetRow = normalize(iterator.next());
                learnPattern(dataSetRow);
            }
            count++;
            neighborhoodRadius=neighborhoodRadius*Math.exp(-((double)count)/((double)iteration));
            learningRate=learningRate*Math.exp(-((double)count)/((double)iteration));
        }
        System.out.println("Teaching completed...");
        System.out.println();
    }

    private DataSetRow normalize(DataSetRow dataSetRow){
        double[] input = dataSetRow.getInput();
        double k = 0.0;
        for(int i = 0; i<input.length;i++)
             k += Math.pow(input[i],2);
        double l = Math.sqrt(k);
        for(int i = 0; i<input.length;i++)
            input[i] = input[i]/l;
        DataSetRow dataSetRow1 = new DataSetRow();
        dataSetRow1.setInput(input);
        return dataSetRow1;
    }

    private void learnPattern(DataSetRow dataSetRow){
        neuralNetwork.setInput(dataSetRow.getInput());
        neuralNetwork.calculate();
        Neuron winner = getClosestNeuron();
        Layer map = neuralNetwork.getLayerAt(1);
        int indexOfWinner = map.indexOf(winner);

        for(int i = 0; i < map.getNeuronsCount(); i++){
            double neighborhood = gaussNeighborhood(indexOfWinner, i);
            changeWeights(map.getNeurons()[i],neighborhood);
        }
    }

    private void changeWeights(Neuron neuron, double neighborhood){
        for(Connection conn : neuron.getInputConnections()) {
            double dWeight = learningRate*neighborhood*(conn.getInput() - conn.getWeight().getValue());
            conn.getWeight().inc(dWeight);
        }
    }

    double rectangleNeighborhood(int indexOfWinner, int i){
        double k = Math.abs(indexOfWinner-i);
        if(k > neighborhoodRadius) return 0.0;
        else return 1.0;

    }

    double gaussNeighborhood(int indexOfWinner, int i){
        double result = Math.exp(-Math.pow(indexOfWinner-i,2)/(2*Math.pow(neighborhoodRadius,2)));
        return result;
    }

    private Neuron getClosestNeuron() {
        Neuron winner = new Neuron();
        double max = 0;
        for(Neuron n: this.neuralNetwork.getLayerAt(1).getNeurons()){
            if(n.getOutput() > max){
                max = n.getOutput();
                winner = n;
            }
        }
        return winner;
    }

    public void setIteration(int iteration){
        this.iteration = iteration;
    }
    public int getIteration(){
        return iteration;
    }
    public double getLearningRate() {
        return learningRate;
    }

    public void setLearningRate(double learningRate) {
        this.learningRate = learningRate;
    }

}
