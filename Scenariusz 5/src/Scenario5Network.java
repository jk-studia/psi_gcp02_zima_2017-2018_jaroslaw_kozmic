import org.neuroph.core.data.BufferedDataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.nnet.Kohonen;

import java.util.ArrayList;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

public class Scenario5Network {

    public static void main(String[] args){
        File file = new File("data.txt");
        File file1 = new File("test.txt");
        BufferedDataSet bufferedDataSet;
        try {
            bufferedDataSet = new BufferedDataSet(file, 4,  10,";");
            WTALearningRule wtaLearningRule = new WTALearningRule();
            Kohonen kohonen = new Kohonen(4, 10);
            kohonen.randomizeWeights();
            kohonen.setLearningRule(wtaLearningRule);
            kohonen.learn(bufferedDataSet);

            bufferedDataSet = new BufferedDataSet(file1, 4, 10, ";");
            List<Double> winners = new ArrayList<>();
           for(DataSetRow r: bufferedDataSet.getRows()){
                kohonen.setInput(r.getInput());
                kohonen.calculate();
                double[] output = kohonen.getOutput();
                double tmp = 0.0;
                double winner = 0.0;
                for(int i = 0; i < output.length; i++){
                    if(output[i] > tmp){
                        tmp = output[i];
                        winner = i;
                    }
                }
               winners.add(winner);
            }
            for(double d: winners){
               System.out.println(d);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
